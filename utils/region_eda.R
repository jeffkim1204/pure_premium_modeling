# ##############################
# # Functions for EDA on region
# ##############################
# 
# returns a dictionary of regions to clusters
cluster_regions = function(df_region_freq, K){
  k2 = kmeans(df_region_freq$avg_freq, centers=K, nstart = 25)
  df_region_freq$region_cluster = k2$cluster

  # create dictionary of region:cluster_index
  cluster_dict = c()
  for (region in df_region_freq$region){
    cluster = df_region_freq[df_region_freq$region == region, ]$region_cluster
    cluster_dict[region] = cluster
  }
  return (cluster_dict)
}

# find the optimal number of clusters
# the num. of clusters that produces a poisson regression model with the largest
# AIC score will determine the number of clusters

determine_num_clusters = function(df, df_region_freq, clusters){

  index = 1
  N = length(clusters)
  aic_ls = numeric(N)

  for (K in clusters){
    # If K == 1, use the original region variable
    if (K == "1"){
      model = glm(formula=clm.count ~ region,
                  family=poisson(link='log'),
                  data=df,
                  offset=log(exposure))
    }

    else{
      cluster_dict = cluster_regions(df_region_freq, as.integer(K))
      df = recode_region(df, cluster_dict)
      model = glm(formula=clm.count ~ region.recode,
                  family=poisson(link='log'),
                  data=df,
                  offset=log(exposure))
    }
    aic_ls[index] = model$aic
    index = index + 1
  }
  return (aic_ls)
}

# function recodes the region variable and attaches it to the dataframe
recode_region = function(df, cluster_dict){
  region_recode = to_vec(for(region in df$region) cluster_dict[region])
  df$region.recode = factor(region_recode)
  return (df)
}

